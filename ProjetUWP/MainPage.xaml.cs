﻿using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ProjetUWP
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //URI API
        private static Uri uri = new Uri("https://restcountries.eu/rest/v2/all?fields=name;flag;alpha2Code");
        private HttpClient client;
        private HttpResponseMessage reponse;

        public MainPage()
        {
            this.InitializeComponent();
            ApplicationView.PreferredLaunchViewSize = new Size(400, 600);
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;

            client = new HttpClient();
            reponse = new HttpResponseMessage();

            //Headers par défaut 
            var headers = client.DefaultRequestHeaders;

            //Listeners
            this.PlayButton.Click += PlayButton_Click;
            this.ClassementButton.Click += ClassementButton_Click;

            //On ne fait la requête GET que s'il s'agit du premier lancement de l'appli
            if (SessionDeJeu.Instance.IsFirstInit)
                GetPaysDrapeaux();
            else
                Debug.WriteLine("On ne reGET pas.");
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            //Crée le joueur
            SessionDeJeu.Instance.joueur = new Classes.Joueur(this.PlayerNameTextBox.Text, 0);
            this.Frame.Navigate(typeof(GamePage));
        }
        private void ClassementButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ClassementPage));
        }

        private async void GetPaysDrapeaux()
        {
            Debug.WriteLine("Début get p&d...");
            //Send the GET request asynchronously and retrieve the response as a string.
            reponse = new HttpResponseMessage();
            String body = "";

            try
            {
                //Send the GET request
                reponse = await client.GetAsync(uri);
                reponse.EnsureSuccessStatusCode();
                body = await reponse.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                body = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
                GetPaysDrapeauxOffline();
                return;
            }
            //Debug.WriteLine(body);
            Debug.WriteLine("\n\nFin");
            
            //On stock le tableau json contenu dans l'objet
            JArray tab = JArray.Parse(body);
            foreach (var v in tab)
            {
                if (v is JToken token)
                {
                    //Debug.WriteLine("\n"+v["flag"]);
                    //Debug.WriteLine(v["name"]);
                    //Debug.WriteLine(v["alpha2Code"].ToString());
                    SessionDeJeu.Instance.AddPays(new Pays(v["name"].ToString(), v["flag"].ToString(), v["alpha2Code"].ToString()));
                }
                else
                    Debug.WriteLine("Token was null (or not a token)");
            }

            //L'app a été lancée, on évite de refaire un get
            SessionDeJeu.Instance.IsFirstInit = false;
        }   

        private void GetPaysDrapeauxOffline()
        {
            for(int i=1; i<6; i++)
            {
                SessionDeJeu.Instance.ListPays.Add(new Pays("Pays" + i, "Pays"+i));
            }
            Console.WriteLine("Fin offline");
            this.Frame.Navigate(typeof(GamePage));
        }

        /*
         * retour à la page d'accueil
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Debug.WriteLine("Hello");
        }
        */
    }
}
