﻿using ProjetUWP.Classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ProjetUWP
{
    public class SessionDeJeu
    {
        public Joueur joueur;
        private Random rnGOD = new Random();
        private static SessionDeJeu instance = null;
        private List<Pays> paysList;
        private int score;
        private bool isFirstInit = true;
        private int lastPick = -1;

        public int LastPick
        {
            get { return lastPick; }
            set { lastPick = value; }
        }
        public bool IsFirstInit
        {
            get { return isFirstInit; }
            set { isFirstInit = value; }
        }

        //Liste des drapeaux (ou plutôt, leur index) que le joueur a déjà vu dans la partie en cours
        private List<int> drapeauxVus;

        public static SessionDeJeu Instance
        {
            get
            {
                if (instance == null)
                    instance = new SessionDeJeu();
                return instance;
            }
        }

        //Init
        private SessionDeJeu()
        {
            drapeauxVus = new List<int>();
            paysList = new List<Pays>();
            score = 0;
        }

        public void AddPays(Pays _p)
        {
            paysList.Add(_p);
        }

        public List<Pays> ListPays
        {
            get { return paysList;  }
        }
        public int Score
        {
            get { return score; }
            set { score = value; }
        }
        public List<int> DrapeauxVus
        {
            get { return drapeauxVus; }
        }
        
        public void AddDrapeauVu(int n) { drapeauxVus.Add(n); }
        public void Reset()
        {
            drapeauxVus = new List<int>();
        }

        public int PickRandom()
        {
            //Tirer un drapeau au hasard dans la "BD"
            int randomInt = rnGOD.Next(0, ListPays.Count);
            Debug.WriteLine("Pick: " + randomInt);

            if (!drapeauxVus.Contains(randomInt))
            {
                drapeauxVus.Add(randomInt);
                lastPick = randomInt;
                return randomInt;
            }
            else
            {
                Debug.WriteLine("Le drapeau " + randomInt + " a déjà était pick, on refait...");
                return PickRandom();
            }
        }

        public Pays[] Pick2FaussesReponses()
        {
            //Tirer un drapeau au hasard dans la "BD"
            int randomInt1 = rnGOD.Next(0, ListPays.Count);
            int randomInt2 = rnGOD.Next(0, ListPays.Count);
            Debug.WriteLine("fauxPick1: " + randomInt1);

            //Une réponse fausse ne peut pas être la bonne réponse
            if(randomInt1 == lastPick || randomInt2 == randomInt1 || randomInt2 == lastPick)
            {
                return Pick2FaussesReponses();
            } else
            {
                Pays[] picks = { ListPays[randomInt1], ListPays[randomInt2] };
                return picks;
            }
        }

        //Réponse correcte ?
        public bool IsCorrectAnswer(string paysName, Pays pays)
        {
            return pays.court.Equals(paysName);
        }
    }
}
