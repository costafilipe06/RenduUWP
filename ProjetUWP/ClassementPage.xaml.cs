﻿using ProjetUWP.Classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Windows.Storage;
using Windows.UI.Xaml.Controls;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace ProjetUWP
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class ClassementPage : Page
    {
        List<Joueur> classementJoueurs;

        public ClassementPage()
        {
            this.InitializeComponent();
            init();
        }

        public async Task GetClassement()
        {
            //Debug.WriteLine("GetY...");


            var sf = await StorageFile.GetFileFromApplicationUriAsync(new Uri(@"ms-appx:///Assets/xml/Classement.xml"));
            //Debug.WriteLine(sf.Path);
            var stream = await sf.OpenStreamForReadAsync();

            XDocument x = XDocument.Load(stream);
            IEnumerable<XElement> joueursXml = x.Element("Players").Elements();

            //Reset la liste
            classementJoueurs = new List<Joueur>();

            //Peupler la liste
            foreach (var player in joueursXml)
            {
                Debug.WriteLine("Adding... ID: " + player.Element("Id").Value + " (score: " + player.Element("Score").Value + ")");
                classementJoueurs.Add(new Joueur(player.Element("Id").Value, Int32.Parse(player.Element("Score").Value)));
            }
            stream.Dispose();
        }

        public async Task init()
        {
            //Update liste
            await GetClassement();

            //Ranger la liste en fonction du score
            classementJoueurs.Sort();
            foreach(Joueur j in classementJoueurs)
            {
                Debug.WriteLine(j.Id + " --> " + j.Score);
            }

            //DataBinding Classement
            ClassementListView.ItemsSource = classementJoueurs;
        }

        private void RetourButtonClassement_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }
    }
}
