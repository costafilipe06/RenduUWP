﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace ProjetUWP
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class GamePage : Page
    {
        private HttpClient clientToEdit;    //Utiliser pour injecter du js dans les pages contenant du svg
        private HttpResponseMessage responseToEdit;
        Pays paysPicked;
        Pays[] mauvaises;
        private Image hidden;
        private WebView hiddenWB;

        const string ResizeJsFunction = "\n<script>" +
            "function resizeSVG(){ " +
            "var svg = document.getElementsByTagName('svg')[0];" +
            //"svg.setAttribute('width', 500);" +
            //"svg.setAttribute('height', 250);" +
            //"svg.setAttribute('transform', 'scale(0.2,0.2)';" +
            "if(svg.getAttribute('width') < 800) { return 'true';} else { return 'false'; }"+
            "}" +
            "</script>";

        public GamePage()
        {
            this.InitializeComponent();
            hidden = new Image();
            hiddenWB = new WebView();
            //this.FlagImage.Navigate(new Uri("ms-appx-web:///Assets/iframe.html"));
            clientToEdit = new HttpClient();

            SessionDeJeu.Instance.Score = 0;
            //UpdateScore(0);

            NouveauDrapeauAsync();



            this.PauseButton.Click += PausePopup;

        }

        private void PausePopup(object sender, RoutedEventArgs e)
        {
            DisplayNoWifiDialog();
        }

        public void NouveauDrapeau_Click(object sender, RoutedEventArgs e)
        {
            NouveauDrapeauAsync();
        }

        public async Task NouveauDrapeauAsync()
        {
            Rep1Button.Background = new SolidColorBrush(Colors.Transparent);
            
            Rep2Button.Background = new SolidColorBrush(Colors.Transparent);

            Rep3Button.Background = new SolidColorBrush(Colors.Transparent);

            //Bonne rep
            paysPicked = SessionDeJeu.Instance.ListPays[SessionDeJeu.Instance.PickRandom()];
            
            //Mauvaises reps
            mauvaises = SessionDeJeu.Instance.Pick2FaussesReponses();

            Debug.WriteLine("Pick: " + paysPicked.Nom + ": " + paysPicked.Image);
            Regex rgx = new Regex(@"( )*\(.*\)");   //Supprimer les espaces et parenthèses
            string result = rgx.Replace(paysPicked.Nom, "").Replace(" ", "-").ToLower();    
            //if (result[result.Length-1].ToString() == ("-"))  //supprime le dernier charactère si c'est un "-" (tiret)
              //  result.Remove(result.Length-1);
            Debug.WriteLine("!!!!!!!    " + result);
            myImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/512/"+ result + ".png", UriKind.Absolute));
#region old
            /*
            string reponse = await clientToEdit.GetStringAsync(new Uri(paysPicked.Image));
            reponse += ResizeJsFunction;
            hiddenWB.NavigateToString(reponse);
            Debug.WriteLine(hiddenWB.Source + "\n0\n" + hiddenWB.ToString());

            bool b = await aaAsync();
            Debug.WriteLine(b);
            while (!b)
            {
                Debug.WriteLine("looping...");

                paysPicked = SessionDeJeu.Instance.ListPays[SessionDeJeu.Instance.PickRandom()];
                Debug.WriteLine("Pick: " + paysPicked.Nom + ": " + paysPicked.Image);

                reponse = await clientToEdit.GetStringAsync(new Uri(paysPicked.Image));
                reponse += ResizeJsFunction;

                b = await aaAsync();
                Debug.WriteLine(b);
            }

            hidden.Source = new SvgImageSource(new Uri(paysPicked.Image));

            this.FlagImage.NavigateToString(reponse);
            
            await FlagImage.InvokeScriptAsync("eval",
                new string[]
                {
                    "resizeSVG()"
                });
            */

            //mySvg.UriSource =new Uri(paysPicked.Image);
            //myImg.Source = mySvg;
            //myImg.Source = new SvgImageSource(new Uri(paysPicked.Image));
            //Debug.WriteLine("AAA");

            //sauvegarder l'img(enfin là c'est l'html)
            /*
            using (HttpClient webClient = new HttpClient())
            {
                byte[] data = await webClient.GetByteArrayAsync(paysPicked.Image);
                StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
                Debug.WriteLine("@@@@@@@@@@" + storageFolder.Path);
                StorageFile sampleFile = await storageFolder.CreateFileAsync(paysPicked.Nom+".png", CreationCollisionOption.ReplaceExisting);
                await FileIO.WriteBytesAsync(sampleFile, data);

            }
            */
            //this.FlagImage.Source = new Uri(paysPicked.Image);

            #endregion

            MelangeRep();
        }
        /*
        private async void getWebViewSize()
        {
            string width = await FlagImage.InvokeScriptAsync("eval", new[] { "document.body.scrollWidth.toString()" });
            Console.WriteLine("width: " + width);
            string height = await FlagImage.InvokeScriptAsync("eval", new[] { "document.body.scrollHeight.toString()" });
            Console.WriteLine("height: " + height);
        }*/
       
        private async Task<bool> aaAsync()
        {
            var v = await hiddenWB.InvokeScriptAsync("eval",
            new string[]
            {
                            "resizeSVG()"
            });
            Debug.WriteLine("!!!!!!!!!!! " + v);
            if(v=="true" || true)
            {
                return true;
            } else
            {
                return false;
            }
        }

        //Mélanger les réponses
        private void MelangeRep()
        {
            Pays[] picks= { mauvaises[1], paysPicked, mauvaises[0]};

            Random r = new Random();
            int rep1 = r.Next(3);
            Debug.WriteLine("r1: " + rep1);

            int rep2 = -1;
            rep2 = r.Next(3);
            while (rep2 == rep1)
            {
                rep2 = r.Next(3);
            }
            Debug.WriteLine("r2: " + rep2);

            int rep3 = -1;
            rep3 = r.Next(3);
            while (rep3 == rep2 || rep3 == rep1)
            {
                rep3 = r.Next(3);
            }
            Debug.WriteLine("r3: " + rep3);

            this.Rep1Button.Content = picks[rep1].court;
            this.Rep2Button.Content = picks[rep2].court;
            this.Rep3Button.Content = picks[rep3].court;
        }

        private void CheckRep_Click(object sender, RoutedEventArgs e)
        {
            Button buttonClicked = (Button)sender;
            string nomRep = (string)buttonClicked.Content;
            Debug.WriteLine("Content: " + nomRep);
            
            //Bonne réponse
            if(SessionDeJeu.Instance.IsCorrectAnswer(nomRep, paysPicked))
            {
                buttonClicked.Background = new SolidColorBrush(Colors.Green);
                buttonClicked.FocusVisualSecondaryBrush = new SolidColorBrush(Colors.Green);
                buttonClicked.FocusVisualPrimaryBrush = new SolidColorBrush(Colors.Green);
                buttonClicked.UseSystemFocusVisuals = false;

                //+5 au score
                UpdateScore(SessionDeJeu.Instance.Score + 5);

                // open the Popup if it isn't open already 
                PopupText.Foreground = new SolidColorBrush(Colors.Green);
                PopupText.Text = "BRAVO";
                if (!StandardPopup.IsOpen) { StandardPopup.IsOpen = true; }

                DispatcherTimer timer = new DispatcherTimer()
                {
                    Interval = TimeSpan.FromMilliseconds(400)
                };
                timer.Tick += delegate (object s, object ee)
                {
                    timer.Stop();
                    if (StandardPopup.IsOpen) StandardPopup.IsOpen = false;
                    NouveauDrapeauAsync();
                };
                timer.Start();

            }
            else
            {   //Perdu
                //buttonClicked.UseSystemFocusVisuals = false;
                buttonClicked.Background = new SolidColorBrush(Colors.Red);
                //buttonClicked.Foc = new SolidColorBrush(Colors.Red);
                buttonClicked.FocusVisualSecondaryBrush = new SolidColorBrush(Colors.Red);
                //buttonClicked.IsFocusEngaged = false;

                // open the Popup if it isn't open already 
                PopupText.Foreground = new SolidColorBrush(Colors.Red);
                PopupText.Text = "Dommage";
                if (!StandardPopup.IsOpen) { StandardPopup.IsOpen = true; }

                DispatcherTimer timer = new DispatcherTimer()
                {
                    Interval = TimeSpan.FromMilliseconds(400)
                };
                timer.Tick += delegate (object s, object ee)
                {
                    timer.Stop();
                    

                    if (StandardPopup.IsOpen) StandardPopup.IsOpen = false;
                    this.Frame.Navigate(typeof(GameOverPage));
                };
                timer.Start();
                //Task.Delay(700).ContinueWith(t => this.Frame.Navigate(typeof(GameOverPage)));

            }
        }

        private void UpdateScore(int newScore)
        {
            //MaJ Score SessionDeJeu + MaJ UI
            SessionDeJeu.Instance.Score = newScore;
            this.ScorePoints.Text = SessionDeJeu.Instance.Score.ToString();
        }



        private async void DisplayNoWifiDialog()
        {
            ContentDialog locationPromptDialog = new ContentDialog
            {
                Title = "PAUSE",
                Content = "Que faire ?",
                CloseButtonText = "Reprendre",
                PrimaryButtonText = "Abandonner"

            };


            ContentDialogResult result = await locationPromptDialog.ShowAsync();
            // Abandonner la partie 
            /// ou Reprendre
            if (result == ContentDialogResult.Primary)
            {
                // Abandonner la partie
                this.Frame.Navigate(typeof(MainPage));
            }
            else
            {
                // The user clicked the CLoseButton, pressed ESC, Gamepad B, or the system back button.
                // Do nothing.
            }
        }

    }
}
//Animated flag: https://www.google.fr/search?dcr=0&biw=1366&bih=652&tbm=isch&sa=1&ei=qT0dWrDTGsHsUMz4qugJ&q=animated+country+flags&oq=animated+country+flags&gs_l=psy-ab.3..0i30k1.16824.16824.0.16977.1.1.0.0.0.0.115.115.0j1.1.0....0...1c..64.psy-ab..0.1.115....0.z2QhqjW56lI#imgrc=LPRjIyXisQLaNM:
//https://restcountries.eu/rest/v2/all?fields=name;flag + capital si voulu