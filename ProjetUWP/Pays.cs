﻿using System.Text.RegularExpressions;

namespace ProjetUWP
{
    public class Pays
    {

        static Regex rgx = new Regex(@"\(.*\)");   //Supprimer les parenthèses
        private string nom;
        public string court;
        private string imgUri;
        private string code2;
        public string fileNameCode2;
        public string Code2
        {
            get { return Code2; }
        }
        public string Image
        {
            get { return imgUri; }
        }
        public string Nom
        {
            get { return nom; }
        }

        public Pays(string _nom)
        {
            nom = _nom;
            court = rgx.Replace(Nom, "");
        }
        public Pays(string _nom, string _imgUri)
        : this(_nom) {
            imgUri = _imgUri;
        }
        public Pays(string _nom, string _imgUri, string _code2)
        :   this(_nom, _imgUri){
            code2 = _code2;
            fileNameCode2 = _code2.ToLower() + "_64.png";
        }

        public override string ToString()
        {
            return "Pays: " + nom + " (drapeau url: " + imgUri + ")";
        }
    }
}
