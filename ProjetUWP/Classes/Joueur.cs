﻿using System;
using System.Collections;

namespace ProjetUWP.Classes
{
    public class Joueur : IComparable<Joueur>
    {
        public string Id { get; set; }
        public int Score { get; set; }

        public Joueur(string _id, int _score)
        {
            Id = _id;
            Score = _score;
        }

        public Joueur()
        {
        }

        #region IComparable<Joueur>
        //Classement décroissant
        public int CompareTo(Joueur other)
        {
            if (this.Score < other.Score)
                return 1;
            else if (this.Score > other.Score)
                return -1;
            else
                return 0;
        }
        #endregion

        public override string ToString()
        {
            return Id + ": " + Score;
        }
    }
}
